module.exports = {
  pages: {
    index: {
      entry: './src/pages/index/main.js',
      template: './public/index.html',
      title: 'index'
    },
    p1: {
      entry: './src/pages/p1/main.js',
      template: './public/index.html',
      title: 'p1'
    },
    p2: {
      entry: './src/pages/p2/main.js',
      template: './public/index.html',
      title: 'p2'
    },
    p3: {
      entry: './src/pages/p3/main.js',
      template: './public/index.html',
      title: 'p3'
    },
    p4: {
      entry: './src/pages/p4/main.js',
      template: './public/index.html',
      title: 'p4'
    }
  }
}
